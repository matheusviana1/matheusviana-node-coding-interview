export function getTwoIndexes(array: number[], target: number) {
    for (let i = 0; i < array.length; i++) {
        let currentValue = array[i]

        for (let j = i + 1; j < array.length; j++) {
            if (currentValue + array[j] === target) {
                return [i, j]
            }
        }
    }

    return []
}
