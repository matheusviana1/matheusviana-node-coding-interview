import assert from 'assert'
import { getTwoIndexes } from '../src/index'

describe('getTwoIndexes', async () => {
    it('check getTwoIndexes with number next to each other', async () => {
        const numbers: number[] = [2, 7, 11, 15]
        const target = 9

        const result = getTwoIndexes(numbers, target)

        return assert.deepEqual(result, [0, 1])
    })
})
